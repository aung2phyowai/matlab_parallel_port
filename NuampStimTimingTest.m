clear;close all;clc;

addpath(genpath('J:\_AungAung-Development\ParallelPortKit\64bit'));
addpath(genpath('J:\_AungAung-Development\matlab_toolbox\hat'));
%% Set up parallel port
%initialize the inpoutx64 low-level I/O driver
config_io;

%optional step: verify that the inpoutx64 driver was successfully installed
global cogent;
if( cogent.io.status ~= 0 )
   error('inp/outp installation failed');
end

address_eros = hex2dec('CC00'); %address of parallel port

stim = [1:127];
%delays = [0.015, 0.02, 0.03, 0.05, 0.1]; %seconds
delays = [0.1, 0.2, 0.3, 0.4, 0.5]; %seconds

matlab_delay = zeros(length(delays),length(stim));
start_time = zeros(length(delays));

for j=1 : length(delays)
    pause(5); %1 second delay between each delay
    start_time(j) = hat;
    for i=1:length(stim)
        time= hat;
        outp(address_eros, stim(i));  %send data one by one
        pause(0.005);
        outp(address_eros, 0);
        pause(delays(j));    %specified delay
        matlab_delay(j, i) = hat - time;
    end
end

save('matlab_delay_2.mat','matlab_delay');
save('matlab_start_time_2.mat','start_time');
